import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('array filter test', () => {
    // Please add test cases here
    test("filterEvenNumber: should filter even numbers", () => {
        //GIVEN
        const number = [2, 3, 4];
    
        //WHEN
        const result = filterEvenNumbers(number);
    
        //THEN
        expect(result.length).toBe(2);
        expect(result).toStrictEqual([2, 4]);
    });

    test("filterLengthWith4: should filter words with length equals to 4", () => {
        // GIVEN
        const words = ["Hello", "good", "morning"];

        // WHEN
        const result = filterLengthWith4(words);

        // THEN
        expect(result.length).toBe(1);
        expect(result).toStrictEqual(["good"]);
    })

    test("filterStartWithA: should filter words starts with 'a'", () => {
        // GIVEN
        const letters = ["Apple", "apple", "animal", "banana"];

        // WHEN
        const result = filterStartWithA(letters);

        // THEN
        expect(result.length).toBe(2);
        expect(result).toStrictEqual(["apple", "animal"]);
    })
});
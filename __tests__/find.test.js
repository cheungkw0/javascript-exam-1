import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";

describe("array find test", () => {
  // Please add test cases here
  test("firstGrownUp: should find the first element that is greater than or equals to 18", () => {
    // GIVEN
    const ages = [15, 16, 18, 17, 20];

    // THEN
    const result = firstGrownUp(ages);

    // THEN
    expect(result).toBe(18);
  })

  test("firstOrange: should find the first orange", () => {
    // GIVEN
    const fruit = ["apple", "orange", "banana"];

    // THEN
    const result = firstOrange(fruit);

    // THEN
    expect(result).toBe("orange");
  })

  test("firstLengthOver5Name: should find the first name that its length is greater than 5", () => {
    // GIVEN
    const names = ["April", "Alexander", "Candice"];

    // THEN
    const result = firstLengthOver5Name(names);

    // THEN
    expect(result).toBe("Alexander");
  })
});

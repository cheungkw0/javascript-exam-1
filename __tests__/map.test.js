import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe("array map test", () => {
  // Please add test cases here
  test("halfNumbers: should return an array of half number", () => {
    // GIVEN
    const numbers = [1, 2, 3];

    // WHEN
    const result = halfNumbers(numbers);

    // THEN
    expect(result).toStrictEqual([0.5, 1, 1.5]);
  });

  test("spliceNames: should return an array of splice names", () => {
    // GIVEN
    const names = ["Alexander", "April", "Candice"];

    // WHEN
    const result = spliceNames(names);

    // THEN
    expect(result).toStrictEqual(["Hello Alexander", "Hello April", "Hello Candice"]);
  });

  test("addSerialNumber: should return an array prefixed by serial number", () => {
    // GIVEN
    const fruit = ["apple", "Pineapple", "watermelon"];

    // WHEN
    const result = addSerialNumber(fruit);

    // THEN
    expect(result).toStrictEqual(["1. apple", "2. Pineapple", "3. watermelon"]);
  });
});
